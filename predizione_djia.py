# -*- coding: utf-8 -*-
"""Predizione_DJIA.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/13Qf9Z9IIataEZqDe92jcxZLv4cIOSQhC

#PREDIZIONE DELL'INDICE DI BORSA DOW JONES

Progetto preliminare all'esame di **Programmazione di Applicazioni Data Intensive**.   
Laurea in Ingegneria e Scienze Informatiche   
DISI - Università di Bologna, Cesena

Anno Accademico 2019-2020

Ameri Keivan  
Matricola: 838804   
`keivan.ameri@studio.unibo.it `

##DESCRIZIONE DEL PROBLEMA

Si vuole realizzare un modello per predirre l'indice di Borsa **Dow Jones**.    

La **Dow Jones Industrial Average**, insieme a **NASDAQ Composite** and **S&P 500**, è uno dei tre indici azionari più seguiti dello stock markets statunitense.   
Gli *indici azionari* (o Stock Indices) sintetizzano il valore di un paniere di azioni e le sue variazioni nel tempo.   
Nel nostro caso, Dow Jones è il paniere che racchiude le azioni di 30 tra le più importanti società americane.
* es. 	McDonald's Corporation, The Coca-Cola Company, 	Apple Inc, ...

    
L'obiettivo è quello di ottenere modelli, tramite i quali è possibile massimizzare il guadagno ottenibile tramite operazioni di **trading intraday**.   
L'intraday o **Day trading** consiste nel comprare e vendere prodotti finanziari all'interno dello stesso giorno. Cioè tutte le posizioni saranno chiuse entro la chiusura della borsa.  
Il Day Trading non è da confondere con lo **Scalping**, ovvero l'apertura e la chiusura di posizioni su vari prodotti finanziari, in un brevissimo arco temporale, dell'ordine di qualche minuto. Tale strategia è nota come "pip and run".   


Nello specifico si vuole predirre il valore della variabile continua che rappresenta il valore di chiusura del'indice in una determinata giornata.    
Il problema è un problema di **regressione**.   

Per raggiungere l'obiettivo, vengono usati i dati di mercato relativi a Dow Jones, resi disponibili dal sito `Yahoo! Finance`, grazie ai quali alleniamo i modelli.

##DATI DEL PROBLEMA

Installiamo **yfinance**, libreria per l'estrazione di dati dal sito `Yahoo! Finance`.
"""

pip install yfinance lxml

"""Importiamo le librerie necessarie."""

# Commented out IPython magic to ensure Python compatibility.
import yfinance
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
# %matplotlib inline
from sklearn.linear_model import LinearRegression, Ridge, Lasso, ElasticNet
from sklearn.kernel_ridge import KernelRidge
from sklearn.preprocessing import PolynomialFeatures, StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.model_selection import TimeSeriesSplit, KFold, cross_validate, GridSearchCV
from sklearn.metrics import make_scorer
from scipy import stats

"""### Raccolta dati

Creiamo un oggetto Ticker. 
- Il `ticker` è un’abbreviazione che identifica le società che vengono quotate su un mercato finanziario;
- Quando una società decide di ‘diventare pubblica’, sceglie il tipo di mercato sui cui essere quotata e il ticker di identificazione.
"""

djia = yfinance.Ticker("^DJI")

"""Tramite il metodo `history` del Ticker, scarico i tutti i dati che si riferiscono al nostro indice.
- I dati sono relativi ad un intervallo di 15 anni. Più precisamente vanno dal `2005-01-01` al `2020-01-01`
- `actions=False` esclude alcune colonne con dati non disponibili per l'indice
"""

data = djia.history(start="2005-01-01", end="2020-01-01", actions=False)
data.head()

data.tail()

print("Numero righe: ", len(data))

"""- Ciascuna riga corrisponde ad una data, indicata nell'indice `Date`
  - le righe sono ordinate secondo un ordine crescente rispetto alla data
  - non sono presenti le righe relative a giorni di chiusura del mercato. Infatti, nonstante il periodo che stiamo considerando è di 15 anni, abbiamo i dati relativi solamente a 3775 date.
- `Open` riporta per ogni data il valore dell'indice all'apertura del mercato
- `Close` riporta per ogni data il valore dell'indice alla chiusura del mercato
- `High` riporta i massimi giornalieri
- `Low` riporta i minimi giornalieri
- `Volume` è il numero di titoli scambiati in giornata

- `Open`, `Close`, `High` e `Low` sono valori in USD (dollaro statunitense)

###Analisi esplorativa

Utilizzo il metodo `info`, per vedere il numero di istanze non nulle, il tipo delle feature e la memoria utilizzata.
"""

data.info(memory_usage="deep")

"""- La memoria occupata dal dataframe è pari a 117.9 KB

Utilizzo il metodo `describe`. Esso fornisce rapidamente un'insieme di statistiche sui valori di ciascuna colonna, utili ad analizzarne la distribuzione
"""

data.describe()

"""La tabella ottenuta mostra:
- `count` indica i valori non mancanti (3775)
- `mean` indica la media
  - ad esempio il valore di apertura medio durante i 10 anni presi in considerazione è 17601,997723
- `std` indica la deviazione standard
- `min` indica i valori minimo
- `max` indica i valori minimo/massimo
- `25%`/`50%`/`75%` inidcano i percentili
  - ad es. durante 25% dei giorni, si ha avuta un valore di apertura inferiore ai 12977.190 dollari

Estraggo le statistiche suddivise per anno, per quanto riguarda l'apertura e la chiusura
"""

data["Open"].groupby(data["Open"].index.year).describe()

data["Close"].groupby(data["Close"].index.year).describe()

"""Ora estraggo i valori della colonne del nostro DataFrame"""

open_values = data["Open"]
close_values = data["Close"]
high_values = data["High"]
low_values = data["Low"]
volume_values = data["Volume"]

"""A seguire vengono mostrati dei grafici"""

open_values.plot(figsize=(20,5))

close_values.plot(figsize=(20,5))

"""Visti in questo modo, è difficile notare differenze tra il grafico dei valori di apertura e quello dei valori di chiusura. Per questo motivo nel seguente grafico vengono messi a confronto i valori di apertura e di chiusura in un intervallo di date ristretto
- I valori considerati vanno dal `2019-12-01` al `2020-01-01`
"""

last_month_open_values = open_values["2019-12-01":"2020-01-01"]
last_month_close_values = close_values["2019-12-01":"2020-01-01"]

figure = plt.figure(figsize=(20, 5))
last_month_open_values.plot(label="Open values")
last_month_close_values.plot(label="Close values")
figure.legend()

"""Posso notare che il valore di apertura di una giornata, non corrisponde al valore di chiusura del giorno precedente

Infine, visualizziamo il rapporto che c'è tra il valore di apertura e chiusura dello stesso giorno, tramite un grafico di dispersione
- I valori sono relativi all'intervallo di date considerato precedentemente
"""

plt.scatter(last_month_open_values, last_month_close_values)
plt.xlabel("Open value")
plt.ylabel("Close value")
plt.show()

"""##ALGORITMO DI DAY TRADING

Il nostro algoritmo per effettuare Day Traiding, ripreso dalla lezione di laboratorio relativa agli indici di borsa, funziona nel seguente modo:

- Dato che ad inizio giornata sappiamo il valore di apertura dell'indice, tramite il nostro modello prevederemo quello di chiusura della giornata stessa 

  - Se il nostro modello ha previsto un rialzo per la chiusura dell'indice, allora acquisteremo un titolo, per poi rivenderlo a fine giornata

  - Se il nostro modello ha previsto un ribasso per la chiusura dell'indice, venderemo il titolo, per poi riacquistarlo a fine giornata

Per migliorare l'efficienza della nostra predizione, definiamo `delta_values`, che corrisponderà alla differenza tra il valore di chiusura e il valore di apertura di una giornata. I valori di `delta_values` saranno reali e predetti.
"""

delta_values = close_values - open_values

"""###Features

Oltre a `delta_values`, `high_values` e `low_values`, andiamo a considerare altre features. Queste features che andiamo a considerare sono legate ai valori di High, Low e Close.   
Le features sono
- `close_oscillation_values`, ovvero un valore che più è vicino a zero e più significa che i valori che l'indice ha assunto durante la giornata non si discostano molto dal valore di chiusura della giornata stessa
- `open_oscillation_values`, ovvero un valore che più è vicino a zero e più significa che i valori che l'indice ha assunto durante la giornata non si discostano molto dal valore di apertura della giornata stessa
"""

high_close_delta_values = high_values.shift(1) - close_values.shift(1)
close_low_delta_values = close_values.shift(1) - low_values.shift(1)
high_open_delta_values = high_values.shift(1) - open_values.shift(1)
open_low_delta_values = open_values.shift(1) - low_values.shift(1)

close_oscillation_values = abs(high_close_delta_values.shift(1) - close_low_delta_values.shift(1))
open_oscillation_values = abs(high_open_delta_values.shift(1) - open_low_delta_values.shift(1))

"""Questi due valori ci possono dare un idea di quanto il valore dell'indice sia stabile durante il corso di una giornata. Questo perchè se entrambi i valori sono molto bassi, significa che il valore dell'indice non ha avuto grandi oscillazioni

Visualizziamo dei grafici relativi alle features enunciate precedentemente
"""

delta_values.plot(figsize=(20,5))

figure = plt.figure(figsize=(20, 5))
high_values.plot(label="High values")
low_values.plot(label="Low values")
figure.legend()

figure = plt.figure(figsize=(20, 5))
open_oscillation_values.plot(label="Open Oscillation")
close_oscillation_values.plot(label="Close Oscillation")
figure.legend()

"""Studiamo la correlazione tra `delta_values` e  `open_oscillation_values`. Vogliamo vedere se a valori basi di uno, corrispondo valori bassi dell'altro   
- Considereremo i valori assoluti dei valori di `delta_values`
"""

plt.scatter(open_oscillation_values, abs(delta_values))
plt.xlabel("Oscilation absolute values")
plt.ylabel("Delta absolute values")
plt.show()

"""Vedendo il grafico, si può notare un'alta densità di punti nell'angolo sinistro in basso. Da ciò possiamo dedurre che esiste una correlazione tra le feature considerate

Possiamo effettuare un discroso analogo per quanto riguarda lo studio della correlazione tra `delta_values` e `open_oscillation_values`
"""

plt.scatter(close_oscillation_values, abs(delta_values))
plt.xlabel("Oscilation absolute values")
plt.ylabel("Delta absolute values")
plt.show()

"""Per studiare al meglio la correlazione tra le features, ora consideriamo i valori corrispondenti ad un periodo di due mesi, che va dal `2019-11-01` al `2020-01-01`.  
Mostriamo la correlazione tramite un grafico
- Nell'analizzare il grafico bisogna ricordarsi che `open_oscillation_values` e `close_oscillation_values` si riferiscono al giorno precedente
"""

figure = plt.figure(figsize=(20, 5))
open_oscillation_values["2019-11-01":"2020-01-01"].plot(label="Open oscillation values")
close_oscillation_values["2019-11-01":"2020-01-01"].plot(label="Close oscillation values")
abs(delta_values["2019-11-01":"2020-01-01"]).plot(label="Delta values")
figure.legend()

"""### Funzioni

Definiamo la funzione `gain`. Tale funzione, prende in input i valori reali e predetti di `delta_values`, e ci restituisce il guadagno di un investitore
- `growth` e `decline` sono due serie booleane che indicano, rispettivamente, in quali giorni è previsto un rialzo del valore e in quali un ribasso
"""

def gain(D, D_pred):
    growth = D_pred > 0
    decline = D_pred < 0
    return D[growth].sum() - D[decline].sum()

"""Definiamo una nuova metrica `roi`, che riusa il guadagno `gain` definito sopra e lo rapporta all'investimento medio. **ROI**, Return on Investment, è un indice che ci esprime quanto il nostro capitale, investito in DJIA, ci stia rendendo"""

def roi(D, D_pred):
    mean_open = open_values.reindex_like(D).mean()
    return gain(D, D_pred) / mean_open

"""Definiamo la funzione `prepare_data`, la quale, prendendo in input un dizionario di variabili predittive `features` e una variabile da predire `target`, crea e resistuisce
- Il frame `X` con le variabili predittive e privo delle righe con valori mancanti
- La serie `y` con i valori della variabile da predire corrispondenti alle righe selezionate di `X`

Utilizziamo il metodo `dropna` per eliminare tutte le righe con uno o più valori mancanti, che si sono venute a formare con il metodo `shift`, utilizzato per generare variabili che si riferiscono ai dati dei giorni precedenti
- `inplace=True` indica di modificare X invece di restituire un frame nuovo
"""

def prepare_data(features, target):
    X = pd.DataFrame(features)
    X.dropna(inplace=True)
    y = target.reindex_like(X)
    return X, y

"""Definiamo la funzione `print_eval`, la quale, prendendo in input il frame con le variabili predittive, la serie con i valori della variabile da predire e il modello, stampa le metriche di valutazione del modello"""

def print_eval(X, y, model):
    preds = model.predict(X)
    print("Gain: {:.2f}$".format(gain(y, preds)))
    print(" ROI: {:.3%}".format(roi(y, preds)))

"""###Suddivisione dei dati

- Suddividiamo i dati in **training** e **validation** set   
- Avendo una serie temporale, è comune utilizzare come training i dati fino ad una data specifica e come validation quelli successivi. Per questo motivo definiamo la funzione `split_before_2015` che, dati X e y, restituisca in una tupla con 4 elementi    
  - Un training set X_train e y_train con i dati fino al 2015   
  - Un validation set X_val e y_val con i dati dal 2015 in poi
- La scelta dell'anno 2015 è dovuta al fatto che è l'anno centrale del periodo di dieci anni che stiamo considerando
"""

def split_before_2015(X, y):
    is_train = X.index.year < 2015
    X_train = X.loc[is_train]
    y_train = y.loc[is_train]
    X_val = X.loc[~is_train]
    y_val = y.loc[~is_train]
    return X_train, X_val, y_train, y_val

"""###Features più rilevanti

Creiamo un dizionario `features`
"""

features = {}
for i in range(1, 4):
  features["DeltaLag{}".format(i)] = delta_values.shift(i)
for i in range(1, 4):
  features["HighLag{}".format(i)] = high_values.shift(i)
for i in range(1, 4):
  features["LowLag{}".format(i)] = low_values.shift(i)
for i in range(1, 4):
  features["OpenOscillationLag{}".format(i)] = open_oscillation_values.shift(i)
for i in range(1, 4):
  features["CloseOscillationLag{}".format(i)] = close_oscillation_values.shift(i)

X, y = prepare_data(features, delta_values)
X.head()

X.tail()

"""Per individuare le feature più rilevanti usiamo il metodo di regressione **LASSO**, Least Absolute Shrinkage and Selection Operator
- imposto $α=3$. Il parametro $α$ controlla il peso della regolarizzazione
"""

model = Lasso(alpha=3)
model.fit(X, y)
pd.Series(model.coef_, X.columns)

"""Sulla base di questa analisi, selezioniamo le features per il training dei modelli.  
Scarteremo le features `high_values` e `low_values`
"""

features = {}
for i in range(1, 4):
  features["DeltaLag{}".format(i)] = delta_values.shift(i)
for i in range(1, 4):
  features["OpenOscillationLag{}".format(i)] = open_oscillation_values.shift(i)
for i in range(1, 4):
  features["CloseOscillationLag{}".format(i)] = close_oscillation_values.shift(i)

X, y = prepare_data(features, delta_values)
X.head()

"""##MODELLI

Andiamo a generare diversi modelli di learning applicabili al nostro caso di studio
"""

X_train, X_val, y_train, y_val = split_before_2015(X, y)

"""Incapsuliamo la metrica `roi` in un oggetto scorer con la funzione `make_scorer`. Nel nostro caso settiamo `greater_is_better` a`True` in quanto stiamo cercando i modelli migliori"""

roi_scorer = make_scorer(roi, greater_is_better=True)

"""Creiamo una funzione per la gestione e visualizzazione dei risultati della GridSearch
- Usiamo GreadSearch per la ricerca degli iperparametri migliori   
La funzione `create_gs_and_display_results` crea il modello grid search specificando tutti i parametri, lo addestra e poi ne ritorna il risultato
"""

def create_gs_and_display_results(model, grid):
  gs = GridSearchCV(model, grid, scoring=roi_scorer, cv=tss)
  gs.fit(X, y);
  gs_results = pd.DataFrame(gs.cv_results_)
  return gs_results.sort_values("mean_test_score", ascending=False)

"""###Modello Casuale

Come primo modello consideriamo un modello casuale. L'obiettivo di questo modello è quello di dimostrare cosa succederebbe se si prendessero delle decisioni casuali.
"""

rois = []
for s in range(1000):
    np.random.seed(s)
    preds = np.random.normal(y_train.mean(), y_train.std(), len(y_val))
    rois.append(roi(y_val, preds))

"""Visualizziamo il ROI medio calcolato su 1.000 test con predizioni casuali"""

np.mean(rois)

"""Possiamo notare che il valore del ROI medio è estramamente piccolo, circa `0.97%`. Ciò significa che i modelli casuali non sono affidabili

###K-Fold per serie temporali
"""

tss = TimeSeriesSplit(3)

for i, (train, val) in enumerate(tss.split(X), start=1):
    print("FOLD {}".format(i))
    train_dates = X.index[train]
    val_dates = X.index[val]
    print("Training set da {} a {}".format(train_dates.min(), train_dates.max()))
    print("Validation set da {} a {}".format(val_dates.min(), val_dates.max()))

cv_results = cross_validate(LinearRegression(), X, y, scoring=roi_scorer, cv=tss)

cv_results["test_score"]

cv_results["test_score"].mean()

"""###Regressione ridge"""

ridge_regression = Pipeline([
    ("scale",  StandardScaler()),
    ("regression", Ridge())
])

"""Definiamo la griglia"""

ridge_regression_grid = {
    "regression__alpha": [0.1, 1, 10, 50]
}

"""Creiamo il modello grid search specificando tutti i parametri, lo addestriamo e poi ne analizziamo i risultati ottenuti"""

ridge_regression_result = create_gs_and_display_results(ridge_regression, ridge_regression_grid)
ridge_regression_result

"""###Regressione Polinomiale

Regressione polinomiale con o senza standardizzazione delle feature generate
"""

poly_regression = Pipeline([
    ("poly", PolynomialFeatures(include_bias=False)),
    ("scale", None),
    ("regr", LinearRegression())
])

"""Definiamo la griglia. Consideriamo i gradi del polinomio che vanno da 1 a 10"""

poly_regression_grid = {
    "scale": [None, StandardScaler()],
    "poly__degree": list(range(1, 5))
}

"""Il caso 1 corrisponde alla regressione lineare standard

Creiamo il modello grid search specificando tutti i parametri, lo addestriamo e poi ne analizziamo i risultati ottenuti
"""

poly_regression_result = create_gs_and_display_results(poly_regression, poly_regression_grid)
poly_regression_result.head()

"""Possiamo notare come le migliori configurazioni, indipendentemente dalla standardizzazione, sono quelle con le feature di 3° grado

###Regressione kernel ridge

Per la regressione kernel ridge, usiamo come kernel `rbf`, radial-basis function
"""

kernel_ridge_regression = Pipeline([
    ("scale", None),
    ("regr", KernelRidge(kernel="rbf"))
])

"""Definiamo la griglia"""

kernel_ridge_regression_grid = {
    "scale": [None, StandardScaler()],
    "regr__gamma": [0.0001, 0.001, 0.01, 1],
    "regr__alpha": np.logspace(-3, 2, 6)
}

"""Creiamo il modello grid search specificando tutti i parametri, lo addestriamo e poi ne analizziamo i risultati ottenuti"""

kernel_ridge_regression_result = create_gs_and_display_results(kernel_ridge_regression, kernel_ridge_regression_grid)
kernel_ridge_regression_result.head()

"""##SELEZIONE MODELLI MIGLIORI

Andremo a selezionare un modello per ognuna delle tipologie di regressioni analizzate.  
Nella scelta dei modelli andremo a considerare i valori che assumo in `mean_test_score` e `std_test_score`
"""

scores ={}

"""Per quanto riguarda la **regressione ridge**, il modello migliore è quello con
- `regression__alpha = 0.1`
- Il suo score è `mean_test_score = 0.350961`
- La sua deviazione standard `std_test_score = 0.241241`
"""

scores["ridge"] = 0.350961

"""Per quanto riguarda la **regressione polinomiale**, il modello migliore è quello con
- La standardizzazione è indefferente
- Grado del polinomio `poly__degree = 1`
- Il suo score è `mean_test_score = 0.350961`
- La sua deviazione standard `std_test_score = 0.241241`
"""

scores["poly"] = 0.350961

"""Per quanto riguarda la **regressione kernel ridge**, selezioniamo due modelli.
- Il primo:
  - Standardizzata
  - Gamma `regr__gamma = 0.001`
  - Alpha `regr__alpha = 0.1`
  - Il suo score è `mean_test_score = 0.443158`
  - La sua deviazione standard `std_test_score = 0.268827`

- Il secondo: 
  - Standardizzata
  - Gamma `regr__gamma = 0.001`
  - Alpha `regr__alpha = 0.01`
  - Il suo score è `mean_test_score = 0.390901`
  - La sua deviazione standard `std_test_score = 0.115591`
"""

scores["kernel_ridge_1"] = 0.443158
scores["kernel_ridge_2"] = 0.390901

scores

"""Calcoliamo gli intervalli di confidenza predittivi fissata la confidenza del **95%**"""

def difference_between_two_models(error1, error2, confidence):
    z_half_alfa = stats.norm.ppf(confidence)
    variance = (((1 - error1) * error1) / len(y_val)) + (((1 - error2) * error2) / len(y_val))
    d_minus = abs(error1 - error2) - z_half_alfa * (pow(variance, 0.5))
    d_plus = abs(error1 - error2) + z_half_alfa * (pow(variance, 0.5))
    print("Valore minimo: {}\nValore massimo: {}\n".format(d_minus, d_plus))

ridge_error = 1 - scores["ridge"]
poly_error = 1- scores["poly"]
kernel_ridge_1_error = 1 - scores["kernel_ridge_1"]
kernel_ridge_2_error = 1 - scores["kernel_ridge_2"]

print("Regressione ridge vs regressione polinomiale, intervallo di confidenza:")
difference_between_two_models(ridge_error, poly_error, 0.95)

print("Regressione ridge vs regressione kernel ridge (1), intervallo di confidenza:")
difference_between_two_models(ridge_error, kernel_ridge_1_error, 0.95)

print("Regressione ridge vs regressione kernel ridge (2), intervallo di confidenza:")
difference_between_two_models(ridge_error, kernel_ridge_2_error, 0.95)

print("Regressione polinomiale vs regressione kernel ridge (1), intervallo di confidenza:")
difference_between_two_models(poly_error, kernel_ridge_1_error, 0.95)

print("Regressione polinomiale vs regressione kernel ridge (2), intervallo di confidenza:")
difference_between_two_models(poly_error, kernel_ridge_2_error, 0.95)

print("Regressione kernel ridge (1) vs regressione kernel ridge (2), intervallo di confidenza:")
difference_between_two_models(kernel_ridge_1_error, kernel_ridge_2_error, 0.95)

"""Possiamo notare che gli score del modello della regressione ridge e di quello della regressione polinomiale hanno stessi risultati.  
Visto che siamo interessati ad avere un alto score e al contempo una bassa deviazione(più il valore della deviazione è alto e più rischiamo che, cambiando i dati, le predizioni non siano accurate), possiamo escluderli visto che non hanno ne lo score migliore tra quelli dei modelli selezionati, ne una bassa deviazione.

##MODELLO MIGLIORE

La scelta del modello migliore ricade tra i due modelli della **regressione kernel ridge**.   
La nostra scelta ricade nel secondo. Questo perchè il secondo, a discapito di una differenza di circa 5 punti percentuali rispetto allo score del primo, ha una deviazione nettamente più bassa (differenza di circa 15 punti percentuali tra i due).
"""

result_model = Pipeline([("scale", StandardScaler()),("regr", KernelRidge(kernel="rbf", alpha=0.01, gamma=0.001))])

"""Per finire analizziamo i parametri appresi (coefficienti degli iperpiani)"""

result_model.fit(X, y)
coef = pd.Series(np.dot(X.transpose(), result_model["regr"].dual_coef_), X.columns)

"""Li stampiamo"""

coef

"""Ricordiamo che la variabile che vogliamo predire è `delta_values`


Possiamo notare che non c'è una maggioranza di parametri positivi o negativi, tuttavia se stringiamo il cerchio ai parametri che si riferiscono fino a 2 giorni precedenti, si hanno praticamente solo valori negativi.   
Ciò indica che questi parametri sono inversamente proporzionali alla variabile da predire.
"""